function getSidebar() {
    return document.querySelector("#sidebar")
}

function hideSidebar() {
    resetSidebarAnimation()
    getSidebar().style.animation = "slideSidebar 0.5s ease-in-out reverse backwards"
}

function showSidebar() {
    resetSidebarAnimation()

    const sidebar = getSidebar()
    sidebar.classList.add("sidebar-in")
    sidebar.style.animation = "slideSidebar 0.5s ease-in-out normal forwards"
}

function resetSidebarAnimation() {
    const sidebar = getSidebar()
    sidebar.classList.remove("sidebar-in")
    sidebar.style.animation = "none"
    void sidebar.offsetWidth // Needed to restart animation
}

function toggleSidebar() {
    if (getSidebar().classList.contains("sidebar-in"))
        hideSidebar()
    else
        showSidebar()
}
