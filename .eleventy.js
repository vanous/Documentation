const navigationPlugin = require("@11ty/eleventy-navigation")
const syntaxHighlightingPlugin = require("@11ty/eleventy-plugin-syntaxhighlight")

module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(navigationPlugin)
  eleventyConfig.addPlugin(syntaxHighlightingPlugin)

  eleventyConfig.addPassthroughCopy("assets")
  eleventyConfig.addPassthroughCopy("fonts")

  eleventyConfig.addShortcode("fas_icon", function(name) { return `<span class="fas fa-${name}"></span>` })

  return {
    dir: {
      input: "content"
    }
  }
}
